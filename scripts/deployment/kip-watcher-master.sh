#!/bin/bash

SRC_DIR="/home/mateusz/kip-watcher"
#SRC_BRANCH="master"   # FIXME make use of this variable
DST_DIR="/srv/www/watcher"

echo "Updating Git repository"
cd ${SRC_DIR} && \
sudo -u mateusz sh -c "git reset --hard && git pull --force --no-edit" && \
echo "Copying files to destination folder" && \
cp ./watcher.py ${DST_DIR} && \
cp -r ./scripts ${DST_DIR} && \
chown -R www-data:www-data ${DST_DIR} && \
echo "Done"

