import os
from datetime import datetime
from email.mime.text import MIMEText
from pprint import pformat
from subprocess import Popen, PIPE, STDOUT

from flask import Flask, request

LOG_FILE = os.path.join(os.path.dirname(os.path.realpath(__file__)), "watcher.log")
SCRIPTS_PATH = os.path.join(os.path.dirname(os.path.realpath(__file__)), "scripts")
EMAIL_FROM = "watcher@kip.unicornum.pl"
FALLBACK_EMAIL_ADDRESS = 'dev@neumanny.net'

watcher = Flask(__name__)


@watcher.route("/watcher", methods=['GET', 'POST'])
def process_webhook():
    if not _is_bitbucket(request):
        return "<h1>KiP Watcher</h1><p>Expecting a <a href=\"{}\">Bitbucket's webhook request</a></p>".format(
            "https://confluence.atlassian.com/bitbucket/manage-webhooks-735643732.html")
    event = request.headers.get('X-Event-Key')
    log("{} webhook {} for '{}' event".format(request.headers.get('X-Request-Uuid'),
                                              request.headers.get('X-Hook-Uuid'),
                                              event))
    details = request.get_json()
    try:
        if event == "repo:push":
            _process_repo_push(details)
            return "OK"
        return "Nothing to do"
    except Exception as ex:
        return log("Something went wrong: {}".format(ex))


def _is_bitbucket(req):
    """Check if the incoming request is a webhook from BitBucket"""
    return req.headers.get('Content-Type') == "application/json" and \
           req.headers.get('User-Agent') == "Bitbucket-Webhooks/2.0"


def _process_repo_push(data):
    """Data passed as Push payload, see https://bit.ly/2AgxgOU"""
    log("DEBUG _process_repo_push data={}".format(pformat(data)))
    branches = []
    commit_authors = []
    commit_messages = []
    repo_name = data.get('repository', {}).get('name', "UNKNOWN")
    push_changes = data.get('push', {}).get('changes', [])
    for change in push_changes:
        for commit in change.get('commits', []):
            commit_authors.append(commit.get('author', {}).get('raw', FALLBACK_EMAIL_ADDRESS))
            commit_messages.append(commit.get('message', ''))
        branches.append(change.get('new', {}).get('name', ''))
    script_outputs = []
    for branch in set(branches):
        (out, err) = _run_deployment_script(repo_name, branch)
        script_outputs.append((out, err))
    try:
        _inform_authors(repo_name, set(commit_authors), set(commit_messages), set(script_outputs))
    except Exception as ex:
        log("Authors might have not been informed about the changes: {}".format(ex))
    try:
        _restart_uwsgi()
    except Exception as ex:
        log("uWSGI might have not been restarted: {}".format(ex))


def _run_deployment_script(repo_name, branch):
    script_name = os.path.join(SCRIPTS_PATH, "deployment", repo_name + "-" + branch + ".sh")
    if not os.path.isfile(script_name):
        log("Deployment script for {} branch {} is not defined".format(repo_name, branch))
        return
    log("Calling {} deployment script".format(script_name))
    try:
        proc = Popen(['/usr/bin/sudo ' + script_name], stdout=PIPE, shell=True)
        (out, err) = proc.communicate()
        return out, err
    except Exception as ex:
        log("Deployment script threw an exception: {}".format(ex))
    finally:
        log("Deployment script finished executing for {}".format(repo_name))


def _inform_authors(repo_name, authors, messages, outputs):
    log("Sending information about the changes in {} to {}".format(repo_name, ",".join(authors)))
    msg = MIMEText("This is watcher script informing about deployment results.\n\nRecent commit messages:\n  * {}.\n\n{}\n\n--\nKIP Watcher".format(
        ",\n  * ".join([m.strip() for m in messages]),
        "\n-----\n".join(["stdout:\n{}\n\nstderr:\n{}\n".format(o.decode('utf-8'), e) for (o, e) in outputs])))
    msg["From"] = EMAIL_FROM
    msg["To"] = ", ".join(authors)
    msg["Subject"] = "New version of {} has been deployed".format(repo_name)
    proc = Popen(['/usr/sbin/sendmail', '-t', '-oi'], stdin=PIPE)
    proc.communicate(msg.as_bytes())
    log("Email to {} has been sent".format(",".join(authors)))


def _restart_uwsgi():
    log("Restarting uWSGI service after 5 seconds")
    proc = Popen(['/usr/bin/sudo /usr/local/sbin/restart-uwsgi.sh'], stderr=STDOUT, shell=True)
    proc.communicate()
    log("uWSGI service has been restarted")


def log(msg):
    with open(LOG_FILE, 'a') as f:
        f.write("{} {}\n".format(datetime.now(), msg))


if __name__ == "__main__":
    watcher.run(host='0.0.0.0', debug=True)

