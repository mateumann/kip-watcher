#!/bin/bash

SRC_DIR="/home/kip/src/kip-backend"
SRC_BRANCH="master"   # FIXME make use of this variable
DST_DIR="/srv/www/kip"
#MAIL_TO="konrad.rymczak@gmail.com" # Google nie lubi ovh
MAIL_TO="dev@neumanny.net"

MAIL_HDR_FROM="From: kip@$(hostname -f)"
MAIL_HDR_TO="To: ${MAIL_TO}"
MAIL_HDR_SUBJECT="Subject: kip-backend.sh script says hello"
MAIL_HDR_DATE="Date: $(date -R)"

echo "Updating Git repository"
cd ${SRC_DIR} && \
sudo -u kip sh -c "git reset --hard && git checkout ${SRC_BRANCH} && git pull --no-edit" && \
echo "Copying files to destination folder" && \
rm -rf ${DST_DIR}/{core,reqs,scripts,templates} && \
cp -a {bah,core,manage.py,reqs,scripts,templates} ${DST_DIR} && \
echo "Running installation in ${DST_DIR}" && \
cd ${DST_DIR} && \
. ${DST_DIR}/venv/bin/activate && \
pip install -r reqs/base.txt ;
python manage.py makemigrations core && \
python manage.py migrate && \
python manage.py collectstatic --noinput && \
deactivate
echo "Changing ownership of deployment directory ${DST_DIR}" && \
chown -R www-data:www-data ${DST_DIR} && \
chmod 0775 ${DST_DIR}/public/media && \
find ${DST_DIR}/public/media -type d -exec chmod 0775 {} + && \
find ${DST_DIR}/public/media -type f -exec chmod 0664 {} + && \
#echo "Restarting UWSGI Service" && \
#sleep 5 && systemctl restart uwsgi.service &
echo "Done"

